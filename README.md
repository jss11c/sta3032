# README #

This repository is for STA3032 at FSU. It contains the retyped LaTeX files necessary to complete the assignments in LaTeX.

Here is a preview of Quiz 1

![Screenshot of quiz 1](/quiz_1_preview.png?raw=true "Quiz 1 Preview")
