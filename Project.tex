\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usetikzlibrary{automata,positioning,arrows,matrix,calc}
\usepackage{enumerate}
\usetikzlibrary{arrows,positioning}
\usepackage{etoolbox}

\newcommand{\low}[1]{$_{\text{#1}}$}
\newcommand{\justif}[2]{&{#1}&\text{#2}}
\newcommand{\sol}[1]{{\color{blue}{#1}}}
\newcommand{\finalSol}[1]{\fcolorbox{red}{white}{\textbf{#1}}}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass\ (\hmwkClassInstructor) \  \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}{
    \section{Problem \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

\newenvironment{homeworkProblemWithPoints}[1][-1]{
    \section{Problem \arabic{homeworkProblemCounter} \textbf{(#1 points)}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Final Project}
\newcommand{\hmwkDueDate}{December 4, 2015}
\newcommand{\hmwkClass}{STA 3032}
\newcommand{\hmwkClassTime}{}
\newcommand{\hmwkClassInstructor}{Professor Jennifer Srigley}
\newcommand{\hmwkAuthorName}{Student Name}

%
% Title Page
%

\title{
    \vspace{2in}
    \huge\textmd{\textbf{Florida State University}}\\
	\huge\textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor\ \hmwkClassTime}}
    \vspace{3in}
}

\author{\textbf{\hmwkAuthorName}}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}
\newcommand\textbox[1]{\parbox{.333\textwidth}{#1}
}
%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\ \\\hrule\ \\\\\textbf{\large Solution:}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}

% Compliment \overline
\newcommand{\ol}[1]{\overline{#1}}


\setlength{\fboxrule}{3pt}%

\begin{document}

\maketitle

\pagebreak
\textbf{\underline{Directions:}}\\
Answer the following questions in a PDF file with written explanations and appropriate outputs from R along with the R commands used to produce the outputs (similar to the notes for Ch. 10 and 12). In order to get full credit for the hypothesis tests, you must specify all parts of the test: hypotheses, test statistic, rejection region or  -value, and conclusion. The data for the following problems is provided in "Project Data.xls." Please let me know if you need it in a different format.

\begin{homeworkProblemWithPoints}[10]
A sample of 139 students was randomly divided into three groups, and each group of students used a different method to learn the names of the other students in their group. Group 1 used the "simple name game," where the 1st student states his/her full name, the 2nd student announces his/her name and the name of the 1st student, the 3rd student says his/her name and the names of the first 2 students, etc. Group 2 used the "elaborate name game," a modification of the simple name game where the students not only state their names but also their favorite activity (e.g. sports). Group 3 used "pairwise introduction," where students are divided into pairs and each student must introduce the other member of the pair. One year later, all subjects were sent pictures of the students in their group and asked to state the full name of each. The researchers measured the percentage of names recalled for each student respondent. The data is displayed in the "namegame" tab of the Excel workbook.\\\\\\
\begin{enumerate}[(a)]
	
\item Construct an ANOVA table using R. Is there evidence that the mean percentages of names recalled differ for the three name retrieval methods? Test using $\alpha = .05$ and the appropriate rejection region.\\\\\\
	
\item If appropriate, perform a multiple comparisons analysis to identify significant differences among the methods at $\alpha = .05$. \\

\end{enumerate}
\end{homeworkProblemWithPoints}
\pagebreak
\begin{homeworkProblemWithPoints}[10]
Sociologists often conduct experiments to investigate the relationship between socioeconomic status and college performance. Socioeconomic status is generally partitioned into three groups: lower class, middle class, and upper class. The grade point averages (GPAs) for random samples of seven college freshmen associated with each of the three socioeconomic classes were selected from a university's files at the end of the academic year. The data is recorded in the "gpa" worksheet. \\\\
\begin{enumerate}[(a)]
\item Construct an ANOVA table using R. Is there evidence of differences among the mean freshmen GPAs for the three socioeconomic classes? Test using $\alpha = .01$. \\\\
\item If appropriate, perform a multiple comparisons analysis to identify significant differences among the socioeconomic classes at $\alpha = .01$.
\end{enumerate}
\end{homeworkProblemWithPoints}
\pagebreak
\begin{homeworkProblemWithPoints}[10]
Samples of six different brands of margarine were analyzed to determine the level of physiologically active polyunsaturated fatty acids (PAPFUA, in percentages). The data is displayed in the "margarine" worksheet. \\\\
\begin{enumerate}[(a)]
\item Construct an ANOVA table using R. Is there evidence of differences among the mean PAPFUA percentages for the six brands of margarine? Test using $\alpha = .10$.
\item If appropriate, perform a multiple comparisons analysis to identify significant differences among the brands at $\alpha = .10$.
\end{enumerate}
\end{homeworkProblemWithPoints}
\pagebreak
\begin{homeworkProblemWithPoints}[10]
Real estate investors, home buyers, and home owners often use the appraised value of a property as a basis for predicting sale price. Data on sale prices ($y$) and total appraised values ($x$) of 92 residential properties sold in 1999 in an upscale Tampa, FL neighborhood are located in the "tampalms" worksheet. Both values are in thousands of dollars. \\\\
\begin{enumerate}[(a)]
\item Calculate and interpret the correlation between $x$ and $y$. \\\\
\item Find the least squares estimates of the slope and intercept using R.\\\\
\item Interpret the slope of the least squares line. \\\\
\item Interpret the intercept of the least squares line. Does it have a practical meaning for this application? Explain. \\\\
\item Conduct a test of model utility using $\alpha = .05$. \\\\
\item Use the least-squares model to estimate the mean sale price of a property appraised at \$300,000.\\\\
\end{enumerate}
\end{homeworkProblemWithPoints}
\pagebreak

\begin{homeworkProblemWithPoints}[10]
Astringency is the quality in a wine that makes the wine drinker's mouth feel slightly rough, dry, and puckery. An investigation was conducted to assess the relationship between perceived astringency ($y$) and tannin concentration ($x$) using various analytic methods. The data is provided in the "wine" worksheet where $x$  = tannin concentration by protein precipitation and  $y$ = perceived astringency as determined by a panel of tasters.
\begin{enumerate}[(a)]
\item Produce a scatterplot of the data. Does it appear that a simple linear regression model will be an appropriate fit to the data? Explain.
\item Find the equation of the simple linear regression model for this data with R.
\item Graph the regression line on the scatterplot produced in (a).
\item What proportion of observed variation in perceived astringency is explained by the tannin concentration of the wine?
\item Calculate and interpret a 95\% confidence interval for the slope of the regression line.
\end{enumerate}
\end{homeworkProblemWithPoints}
\end{document}